/**
 * Form and Code Workshop at MMU
 * <p>
 * Position circles in different y
 */

import processing.pdf.*;

int scl = 30;

boolean recordPDF = false;

void setup(){
  size(600,600);
  smooth();
}

void draw(){

  background(200);

  if(recordPDF){
    beginRecord(PDF,"####-pattern.pdf");
  }
  translate(0, height/2);

  for (int i=0; i < width/scl; i++) {
    fill(0);
    float circleSize = sin(i)*scl;
    float y = sin(i)*100;
    ellipse(scl/2+i*scl,y, circleSize, circleSize);
  }
  if (recordPDF){
    endRecord();
    recordPDF = false;
  }

}

void keyReleased() {
  if (key == 'r') {
    recordPDF = true;
  }

}

