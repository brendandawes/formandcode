/**
 * Form and Code Workshop at MMU
 * <p>
 * Extrudes faces
 */

import processing.pdf.*;
import wblut.math.*;
import wblut.processing.*;
import wblut.core.*;
import wblut.hemesh.*;
import wblut.geom.*;
import java.util.Iterator;
import dawesometoolkit.*;
import peasy.*;
import java.util.List;

HE_Mesh mesh;
HE_Mesh originalMesh;
WB_Render render;
HEM_Extrude modifier;
DawesomeToolkit dawesome;
PeasyCam cam;
List<WB_Point> points;

float frequency = 0.093;
float amplitude = 2;

boolean recordPDF = false;

void setup(){

  size(600,600,P3D);
  cam = new PeasyCam(this,800);
  cam.setWheelScale(0.05);
  smooth();
  render=new WB_Render(this);
  createMesh();
  //chngePoints();
  changeFaces();

}

/**
 * creates a mesh
 */

void createMesh(){

  HEC_Sphere creator=new HEC_Sphere();
  creator.setRadius(150);
  creator.setUFacets(32);
  creator.setVFacets(32);
  mesh=new HE_Mesh(creator);
  originalMesh = mesh.get();

}

void smoothMesh(){
  HES_CatmullClark subdividor=new HES_CatmullClark();
  mesh.subdivide(subdividor, 1);
}

void changeFaces() {

  HE_Mesh m = originalMesh.get();
  int amount = m.getNumberOfFaces();
  HEM_Extrude modifier=new HEM_Extrude();
  for (int i=0; i < amount; i++){
    HE_Face face = m.getFaceWithIndex(i);
    HE_Selection selection=new HE_Selection(m);
    selection.add(face);
    float distance = abs(sin(i*frequency)*amplitude)*20;
    modifier.setDistance(distance);
    selection.modify(modifier);
  }

  mesh = m.get();

}

void changePoints() {

  List<WB_Point> points= new ArrayList<WB_Point>(); 
  HE_Mesh m = originalMesh.get();
  int amount = m.getNumberOfVertices();
  for (int i=0; i < amount; i++) {
    WB_Coord coord = m.getVertex(i);
    WB_Point p = new WB_Point(coord.xf(),coord.yf(),coord.zf());
    float scaler = abs(sin(i*frequency)*amplitude);
    p = p.scale(scaler);
    WB_Point newPoint = new WB_Point(p.xf(),p.yf(),p.zf());
    points.add(newPoint);
  }

  mesh.setVerticesFromPoint(points);

}

void draw(){

  background(200);
  stroke(200);
  render.drawEdges(mesh);
  noStroke();
  fill(255);
  render.drawFaces(mesh);

}

void keyReleased() {
  if (key == 'r') {
    mesh.triangulate();
    HET_Export.saveToSTL(mesh,sketchPath(),"my_mesh");
  }

  if (key=='s') {
    smoothMesh();
  }
  if (key=='m') {
    frequency = mouseX/1000.0;
    changeFaces();
  }

}



