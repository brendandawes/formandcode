/**
 * Form and Code Workshop at MMU
 * <p>
 * Interaction
 */

import processing.pdf.*;

int scl = 30;

boolean recordPDF = false;

void setup(){
  size(600,600);
  smooth();
}

void draw(){

  background(200);

  if(recordPDF){
    beginRecord(PDF,"####-pattern.pdf");
  }
  translate(0, height/2);

  for (int j=0; j < 10; j++) {
    for (int i=0; i < width/scl; i++) {
        if (i%2==0){
          fill(0);
          noStroke();
        }else{
       noFill();
       stroke(0);
        }
      float scaler = map(mouseX,0,width,5,100);
      float freq = map(mouseY,0,height,0.01,2.0);
      float circleSize = sin(i*freq)*scaler;
      float y = j*sin(j*freq)*20+sin(i*freq)*20;
      ellipse(scl/2+i*scl,y, circleSize, circleSize);
    }
  }
  if (recordPDF){
    endRecord();
    recordPDF = false;
  }

}

void keyReleased() {
  if (key == 'r') {
    recordPDF = true;
  }

}

