/**
 * Form and Code Workshop at MMU
 * <p>
 * Type
 */

import processing.pdf.*;

int scl = 30;
PFont myFont;

boolean recordPDF = false;

void setup(){
  size(600,600);
  myFont = createFont("OstrichSans-Light.otf",32);
  textFont(myFont);
  textAlign(CENTER, CENTER);
  smooth();
}

void draw(){

  background(200);

  if(recordPDF){
    beginRecord(PDF,"####-pattern.pdf");
    textFont(myFont);
    textAlign(CENTER, CENTER);
  }
  translate(0, height/2);

  for (int j=0; j < 10; j++) {
    for (int i=0; i < width/scl; i++) {
      fill(0);
      float scaler = map(mouseX,0,width,100,1000);
      float freq = map(mouseY,0,height,0.01,2.0);
      float tSize = max(5,sin(i*freq)*scaler);
      float y = j*noise(j*freq)*20+noise(i*freq)*20;
      textSize(tSize);
      text(i,scl/2+i*scl,y);
    }
  }
  if (recordPDF){
    endRecord();
    recordPDF = false;
  }

}

void keyReleased() {
  if (key == 'r') {
    recordPDF = true;
  }

}

