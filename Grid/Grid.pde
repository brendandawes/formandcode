/**
 * Form and Code Workshop at MMU
 * <p>
 * Creates a grid using the Dawesome Toolkit
 */

import dawesometoolkit.*;
import processing.pdf.*;


DawesomeToolkit dawesome;
boolean recordPDF = false;

ArrayList<PVector> layout;

void setup(){
  size(640,640);
  dawesome  = new DawesomeToolkit(this);
  dawesome.enableLazySave('s',".png");
  layout = dawesome.gridLayout(100,20,20,10);
  dawesome.centerPVectors(layout);
  smooth();

}

void draw(){
  if(recordPDF){
    beginRecord(PDF,"####.pdf");
  }

  background(200);
  translate(width/2, height/2);
  fill(0);

  for (PVector p:layout){

    ellipse(p.x, p.y, 10, 10);

  }

  if (recordPDF){
    endRecord();
    recordPDF = false;
  }
}

void keyReleased() {
  if (key == 'r') {
    recordPDF = true;
  }

}




