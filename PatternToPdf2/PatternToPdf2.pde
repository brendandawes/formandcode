/**
 * Form and Code Workshop at MMU
 * <p>
 * Uses a sine wave to alter the shape
 */

import processing.pdf.*;

int scl = 50;

boolean recordPDF = false;

void setup(){
  size(600,600);
  smooth();
}

void draw(){

  background(200);

  if(recordPDF){
    beginRecord(PDF,"####-pattern.pdf");
  }

  for (int i=0; i < width/scl; i++) {
    fill(0);
    float circleSize = sin(i)*scl;
    ellipse(scl/2+i*scl, height/2, circleSize, circleSize);
  }
  if (recordPDF){
    endRecord();
    recordPDF = false;
  }

}

void keyReleased() {
  if (key == 'r') {
    recordPDF = true;
  }

}

